const bind = (el) => {
    el.onmousedown = el.ontouchstart = (evt) => {
        drawing = true;
        draw(evt.pageX - this.offsetLeft, evt.pageY - this.offsetTop);
    }

    el.onmouseup = el.ontouchend = el.onmouseleave = () => {
        drawing = false;
    }

    el.onmousemove = el.ontouchmove = (evt) => {
        evt.preventDefault();
        if(!drawing) return;

        draw(evt.pageX - this.offsetLeft, evt.pageY - this.offsetTop);
    }

    const reset = () => {
        context.clearRect(0, 0, context.canvas.width, context.canvas.height);

        context.beginPath();
        context.rect(0, 0, context.canvas.width, context.canvas.height);
        context.fillStyle = "#FFFFFF"
        context.fill();
    }

    const draw = (x, y) => {
        if(lastX !== undefined && lastY !== undefined) {
            console.log('drawing path');
            context.beginPath();
            context.strokeStyle = "rgba(0, 0, 0, 255)";
            context.lineJoin = "round";
            context.lineWidth = 2;
            context.moveTo(lastX, lastY);
            context.lineTo(x, y);
            context.closePath();
            context.stroke();
        }

        lastX = x;
        lastY = y;
    }

    let drawing = false;
    const context = el.getContext('2d');
    let lastX, lastY = undefined;
    reset();
}

window.onload = () => {
    bind(document.getElementById('canvas'));
}