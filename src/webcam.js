const load = async () => {
    const video = document.getElementById('video');
    const result = document.getElementById('result');
    const bwCanvas = document.getElementById('bw');
    const bwContext = bwCanvas.getContext("2d");
    const cropCanvas = document.getElementById('crop');
    const cropContext = cropCanvas.getContext("2d");
    const interestingCanvas = document.getElementById('interesting');
    const interestingContext = interestingCanvas.getContext("2d");
    const msg = document.getElementById('msg');
    const model = await tf.loadLayersModel('network/digits.json');

    msg.innerText = 'Loading webcam...';

    if (navigator.mediaDevices === undefined) {
        msg.innerText = "Media device unavailable, insecure connection?";
        return;
    }

    const constraints = {audio: false, video: { facingMode: "environment" }};

    navigator.mediaDevices.getUserMedia(constraints)
        .then(function (mediaStream) {
            video.srcObject = mediaStream;
            video.onloadedmetadata = function (e) {
                video.play();
                msg.innerText = 'Webcam is streaming'

                setTimeout(analyse, 20);
            };
        })
        .catch((err) => {
            if (err.name === 'OverconstrainedError') {
                msg.innerText = 'No webcam found. :(';
                return;
            }
            msg.innerText = (err.name + ": " + err.message);
        });


    const analyse = () => {
        const frame = getFrame(video);

        for (let i = 0; i < frame.data.length; i += 4) {
            const pixel = (frame.data[i] + frame.data[i + 1] + frame.data[i + 2]) / 3;
            const norm = (pixel > 92) ? 0 : 255;
            frame.data[i] = norm;
            frame.data[i + 1] = norm;
            frame.data[i + 2] = norm;
        }

        bwContext.putImageData(frame, 0, 0);

        const map = determineInteresting(bwContext, frame.width, frame.height);
        const img = interestingContext.getImageData(0, 0, frame.width, frame.height);

        for (let y = 0; y < img.height; y++) {
            for (let x = 0; x < img.width; x++) {
                const idx = (y * img.width + x) * 4;
                img.data[idx] = map[y][x] * 255;
                img.data[idx + 1] = map[y][x] * 255;
                img.data[idx + 2] = map[y][x] * 255;
                img.data[idx + 3] = 255;
            }
        }

        interestingContext.putImageData(img, 0, 0);

        const [minX, minY, maxX, maxY] = findBoundaries(map);

        cropContext.drawImage(interestingCanvas, minX-4, minY-4, maxX+8 - minX, maxY+8 - minY, 4, 4, 20, 20);

        const cimg = cropContext.getImageData(0, 0, 28, 28);

        const input = new Float32Array(28*28);
        for(let i = 0, p = 0; i < cimg.data.length; i+=4, p++) {
            input[p] = cimg.data[i] / 255;
        }

        const tensor = tf.tensor1d(input).reshape([1, 28, 28, 1]);

        const preds = model.predict(tensor).argMax(-1);

        result.innerText = `This might be a ${preds.dataSync()[0]}`;

        setTimeout(analyse, 0);
    }

}

const main = () => {
    load().then(() => console.log('load resolved'));
}

const determineInteresting = (context, width, height) => {
    const map = [];
    for (let y = 0; y < height; y++) {
        map[y] = [];
        for (let x = 0; x < width; x++) {
            map[y][x] = isPositionInteresting(context, x, y);
        }
    }

    return map;
}

const findBoundaries = (map) => {
    let minX, minY, maxX, maxY;
    for (let y = 0; y < map.length; y++) {
        if (rowHasTrue(map, y)) {
            minY = y;
            break;
        }
    }
    for (let x = 0; x < map[0].length; x++) {
        if (colHasTrue(map, x)) {
            minX = x;
            break;
        }
    }

    if (minX === undefined || minY === undefined) {
        return [0, 0, 10, 10];
    }

    for (let y = map.length - 1; y >= 0; y--) {
        if (rowHasTrue(map, y)) {
            maxY = y;
            break;
        }
    }
    for (let x = map[0].length - 1; x >= 0; x--) {
        if (colHasTrue(map, x)) {
            maxX = x;
            break;
        }
    }

    return [minX, minY, maxX, maxY];
}

const rowHasTrue = (map, y) => {
    for (let x = 0; x < map[y].length; x++) {
        if (map[y][x]) return true;
    }
    return false;
}

const colHasTrue = (map, x) => {
    for (let y = 0; y < map.length; y++) {
        if (map[y][x]) return true;
    }
    return false;
}

const isPositionInteresting = (context, x, y) => {
    const data = context.getImageData(
        x === 0 ? 0 : x - 3,
        y === 0 ? 0 : y - 3,
        6, 6).data;
    let sum = 0;
    for (let i = 0; i < data.length; i += 4) {
        // Only looking at red channel
        sum += (data[i] > 128 ? 1 : 0);
    }

    return sum >= 25;
}

const getFrame = (video) => {
    const canvas = document.createElement("canvas");
    canvas.width = 300;
    canvas.height = 300;
    const canvasContext = canvas.getContext("2d");
    canvasContext.drawImage(video, 0, 0, video.videoWidth, video.videoHeight, 0, 0, 300, 300);
    return canvasContext.getImageData(0, 0, canvas.width, canvas.height);
}

window.onload = main;